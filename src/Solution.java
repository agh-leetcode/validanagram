import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {
        String s = "anagram";
        String t = "nagaram";
        String s1 = "rat";
        String t1 = "car";
        boolean result = isAnagram(s1,t1);
        System.out.println(result);
    }

    private boolean isAnagram(String s, String t) {
        if (s.length()!=t.length())
            return false;

        char[] s1 = s.toCharArray();
        Arrays.sort(s1);
        char[] t1 = t.toCharArray();
        Arrays.sort(t1);
        return Arrays.equals(s1, t1);
    }
}
